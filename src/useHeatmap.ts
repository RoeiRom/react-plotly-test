import React from 'react';

export const useHeatmap = (params: useHeatmapParams): useHeatmapOutcome => {
    
    const { myState, setMyState } = params;
    
    const handleClick = () => {
        if (myState === 1) {
            setMyState(2);
        } else {
            setMyState(1);
        }
    }

    return {
        handleClick
    };
}

interface useHeatmapParams {
    myState: number;
    setMyState: React.Dispatch<React.SetStateAction<number>>;
}

export interface useHeatmapOutcome {
    handleClick: () => void;
}