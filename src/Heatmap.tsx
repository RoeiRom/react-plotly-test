import React from 'react';
// import Plot from 'react-plotly.js';
import Plotly from "plotly.js";

import createPlotlyComponent from "react-plotly.js/factory";
import { useHeatmap } from './useHeatmap';
const Plot = createPlotlyComponent(Plotly);

const Heatmap: React.FC = () => {

    const [myState, setMyState] = React.useState<number>(1);

    const { handleClick } = useHeatmap({myState, setMyState});

    return (
        <>
            <div id="myDiv" style={{ cursor: 'pointer' }} onClick={handleClick}>
                Hello World! {myState}
            </div>
            <Plot
                data={[
                    {
                        x: [1, 2, 3],
                        y: [4, 5, 6],
                        z: [[1, 5, 60], [20, 7, -10], [30, 10, 40]],
                        type: 'heatmap'
                    }
                ]}
                layout={ {width: 320, height: 240, title: 'A Fancy Plot'} }
                config={{
                    displayModeBar: false
                }}
            />
        </>
    )
}

export default Heatmap;