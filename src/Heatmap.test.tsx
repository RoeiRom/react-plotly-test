import React from 'react';
import { shallow } from 'enzyme';
import { act } from 'react-dom/test-utils';

import Heatmap from './Heatmap';
import { useHeatmap, useHeatmapOutcome } from './useHeatmap';
import { testHooksFunction } from './testUtils';

test('find myDiv in the component', () => {
    const wrapper = shallow(<Heatmap />);
    // eslint-disable-next-line
    expect(wrapper.find(<div id='myDiv'>Hello World</div>)).toBeTruthy();
})

describe('useHeatmap tests', () => {
    test('handle click increase to 2', () => {
        let myState: number = 1;
        const setMyState = (newState: number) => {myState = newState;}
        let useHetamapOutcome: useHeatmapOutcome;
        testHooksFunction(() => {
            useHetamapOutcome = useHeatmap({myState, 
                // @ts-ignore
                setMyState});
        });
        act(() => {
            useHetamapOutcome.handleClick();
        });
        expect(myState).toBe(2);
    });

    test('handle click decrease to 1', () => {
        let myState: number = 2;
        const setMyState = (newState: number) => {myState = newState;}
        let useHetamapOutcome: useHeatmapOutcome;
        testHooksFunction(() => {
            useHetamapOutcome = useHeatmap({myState, 
                // @ts-ignore
                setMyState});
        });
        act(() => {
            useHetamapOutcome.handleClick();
        });
        expect(myState).toBe(1);
    });
})