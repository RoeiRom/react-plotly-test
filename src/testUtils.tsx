import React from 'react';
import { mount } from 'enzyme';

const TestHooksComponent = ({ callback }: any): null => {
    callback();
    return null;
}

export const testHooksFunction = (callback: any): void => {
    mount(<TestHooksComponent callback={callback} />);
}